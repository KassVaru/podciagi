# Podciągi

Projekt wykonany na podstawie zadania:

> Mamy dowolny ciąg 10 liczb np. 
> 5 4 6 4 1 5 7 6 8 4 
> Jak wyznaczyć ilu elementowy jest najdłuższy malejący podciąg ?

## Co potrzebujemy:
* [CodeBlocks + MinGW](http://www.codeblocks.org/downloads)
* [Git](https://git-scm.com/downloads) (opcjonalnie)

## Jak pobrać i uruchomić?

1. Pobieramy paczkę zip z [LINK](https://gitlab.com/KassVaru/podciagi/-/archive/master/podciagi-master.zip)
2. Uruchamiamy program CodeBlocks
3. Wypakowujemy i importujemy nasz projekt do CodeBlocks
4. Wciskamy klawisz F8 na klawiaturze
5. Korzystamy z cudowności programu

### Opcjonalnie
Możemy również wykorzystać pakiet GIT, by pobrać najnowszą wersję programu:
1. W konsoli git/cmd wpisujemy: ``` git clone https://gitlab.com/KassVaru/podciagi.git ```
2. Reszta taka sama jak wyżej