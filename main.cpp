#include <iostream>
#include <cstdlib>
#include <time.h>
#include <conio.h>
#include <windows.h>

using namespace std;

int main()
{
    // Define lang and time
    setlocale( LC_ALL, "" );
    srand(time(NULL));
    // Create basic variables
    int a,maks=1,l=1;

    // Type and construct series
    cout<<"Podaj jak d�ugi ma by� ci�g? ";
    cin>>a;

    // Check if a<=1
    if(a<=1){
        cout<<"Prosz� poda� liczb� wi�ksz� od 1";
        Sleep(1500);
        main();
    } else {
        int c[a];

        // Make "c" random variables
        for(int i=0;i<a;i++){
            c[i] = rand()%10+1;
            cout<<c[i]<<endl;
        }

        // Check longest next series
        for(int i=0; i<a; i++){
            if(c[i]>c[i+1]){
                l++;
            } else{
                l=1;
            }
            if(l>maks){
                maks=l;
            }
        }
        cout<<"Najd�u�szy malej�cy podci�g posiada: "<<maks<<" element/y/�w"<<endl;
        main();
    }

}
